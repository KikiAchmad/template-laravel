<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use app\Pertanyaan;

class PertanyaanController extends Controller
{   
    //function menampilkan halaman pertanyaan
    public function index(){
        $list = DB::table('pertanyaan')->get();
        //dd($list);
        return view('pertanyaan.pertanyaan', compact('list'));
    }

    //function menampilkan halaman create
    public function create(){
        return view('pertanyaan.create');
    }

    //function menampilkan halaman berdasarkan data yang dipilih
    public function show($pertanyaan_id){
        $tampil = DB::table('pertanyaan')->where('id', $pertanyaan_id)->first();
        //dd($tampil);
        return view('pertanyaan.tampildetail', compact('tampil'));
    }

    //function untuk edit data berdasarkan data yang dipilih
    public function edit($pertanyaan_id){
        $tampil = DB::table('pertanyaan')->where('id', $pertanyaan_id)->first();
        return view('pertanyaan.editdata', compact('tampil'));
    }

    //function untuk save data yang telah diedit
    public function update($pertanyaan_id, Request $request){
        //validasi jika field kosong
        $request->validate([
            'judul' => 'required|unique:pertanyaan|max:45',
            'isi' => 'required'
        ]);
        //code untuk update data dari request yg telah dibawa    
        $query = DB::table('pertanyaan')
              ->where('id', $pertanyaan_id)
              ->update([
                  'judul' => $request['judul'],
                  'isi' => $request['isi']
              ]);
        return redirect('/pertanyaan')->with('success','data berhasil diubah');
    }

    //function delete data
    public function destroy($pertanyaan_id){
        $query = DB::table('pertanyaan')->where('id', $pertanyaan_id)->delete();
        return redirect('/pertanyaan')->with('success','data berhasil dihapus');
    }

    //function insert atau menyimpan data
    public function store(Request $request){
        
        //validasi jika field kosong
        //dd($request->all());
        $request->validate([
            'judul' => 'required|unique:pertanyaan|max:45',
            'isi' => 'required',
        ]);
        //insert to database
        $query = DB::table('pertanyaan')->insert([
            "judul" => $request["judul"],
            "isi" => $request["isi"]
        ]);
        return redirect('/pertanyaan')->with('success','data berhasil ditambahkan');
    }
}
