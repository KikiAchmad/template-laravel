@extends('layout.master')

@section('content')
    <div class=col-md-8>
        <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Buat Pertanyaan</h3>
            </div>
                <!-- /.box-header -->
                <!-- form start -->
                <div class="col-md-12 mb-2">
                    <form role="form" action="/posts" method="POST">
                    @csrf
                            <div class="box-body">
                                <div class="form-group">
                                    <label for="title">Judul</label>
                                        <input type="text" class="form-control" name="judul" id="judul" value = "{{ old('judul','') }}" placeholder="Masukkan Judul">
                                        @error('judul')
                                        <div class="alert alert-danger">{{ $message }}</div>
                                        @enderror
                                </div>
                                <div class="form-group">
                                    <label>Isi</label>
                                    <input type="text" class="form-control" name="isi" id="isi" value = "{{ old('isi','') }}" placeholder="Masukkan Isi">
                                    @error('isi')
                                    <div class="alert alert-danger">{{ $message }}</div>
                                    @enderror
                                </div>
                            </div>
                            <!-- /.box-body -->
                            <div class="box-footer">
                                <button type="submit" class="btn btn-primary">Buat</button>
                            </div>
                    </form>
                </div>
        </div>
    </div>
@endsection