<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/data-tables','FormController@table'); 

Route::get('/pertanyaan','PertanyaanController@index');

Route::get('/pertanyaan/create','PertanyaanController@create');

Route::post('/posts','PertanyaanController@store');

Route::get('/tampildetail/{pertanyaan_id}', 'PertanyaanController@show');

Route::get('/tampildetail/{pertanyaan_id}/edit', 'PertanyaanController@edit');

Route::put('/save/{pertanyaan_id}', 'PertanyaanController@update');

Route::delete('/delete/{pertanyaan_id}', 'PertanyaanController@destroy');

